var pesquisa = 0;
var id_entrevista = 0;

var localDB = null;
//var raiz = "http://localhost/vox/php/";
var raiz = "http://pesquisas.datamedia.com.br/php/";

function onInit(){
	
	
    try {
        if (!window.openDatabase) {
            updateStatus("Erro: Seu navegador não permite banco de dados.");
        }
        else {
            initDB();
			//irPara("page_menu");
			//irPara("page_admin");
			//irPara("page_pesquisa2");
        }
    } 
    catch (e) {
        if (e == 2) {
            updateStatus("Erro: Versão de banco de dados inválida.");
        }
        else {
            updateStatus("Erro: Erro desconhecido: " + e + ".");
        }
        return;
    }
	

}

function initDB(){
    var shortName = 'datamedia';
    var version = '1.0';
    var displayName = 'Datamedia';
    var maxSize = 65536; // Em bytes
    localDB = window.openDatabase(shortName, version, displayName, maxSize);
}


//EXCLUIR TABELAS
function drop_table(tabela){
	var query = 'drop table '+tabela+";";
    try {
        localDB.transaction(function(transaction){
            transaction.executeSql(query, [], nullDataHandler, errorHandler);
			updateStatus(tabela, "Dados excluídos");
        });
    } 
    catch (e) {
        return;
    }
}

//CRIAR TABELAS
function create_table(tabela, sql){
	var query = sql;
    try {
        localDB.transaction(function(transaction){
            transaction.executeSql(query, [], nullDataHandler, errorHandler);
            updateStatus(tabela, "OK");
        });
    } 
    catch (e) {
        updateStatus(tabela, e);
        return;
    }
}

//LIMPAR DADOS
function delete_data(tabela){
	var query = 'delete from '+tabela+";";
    try {
        localDB.transaction(function(transaction){
            transaction.executeSql(query, [], nullDataHandler, errorHandler);
			updateStatus(tabela, "Dados excluídos");
        });
    } 
    catch (e) {
        return;
    }
}

//INSERIR DADOS
function insert_data(tabela){
	$.post(raiz+"ajax.php?funcao="+tabela,
		function(data) {
			if(data){
				try {
					localDB.transaction(function(transaction){
						var n=data.split(";");
						for(i=0; i<n.length-1; i++){
							//alert(n[i]);
							console.log(n[i]);
							transaction.executeSql(n[i], [], nullDataHandler, errorHandler);
						}
						updateStatus(tabela, i);
					});
				} 
				catch (e) {
					
					updateStatus(tabela, e);
					return;
				}
			} else {
				//alerta("Nenhuma certificado registrado!"); 
			}
		}
	)
	.fail(function() {
		sem_conexao();
	})
}


function excluir_tabelas(){
	$("#lista_log").children().remove();
	drop_table('atendente');
	drop_table('classe');
	drop_table('cliente');
	drop_table('escolaridade');
	drop_table('idade');
	drop_table('religiao');
	drop_table('pergunta');
	drop_table('pesquisa');
	drop_table('resposta');
	drop_table('sexo');
	drop_table('regiao');
	drop_table('entrevista');
	drop_table('entrevista_resultado');
}

function criar_tabelas(){
	$("#lista_log").children().remove();
	create_table('atendente', 'CREATE TABLE IF NOT EXISTS atendente(id_atendente INTEGER NOT NULL PRIMARY KEY, nome VARCHAR NOT NULL, login VARCHAR NOT NULL, senha VARCHAR NOT NULL);');
	create_table('classe', 'CREATE TABLE IF NOT EXISTS classe(id_classe INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, classe VARCHAR NOT NULL);');
	create_table('cliente', 'CREATE TABLE IF NOT EXISTS cliente(id_cliente INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, nome_razao VARCHAR NOT NULL);');
	create_table('escolaridade', 'CREATE TABLE IF NOT EXISTS escolaridade(id_escolaridade INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, escolaridade VARCHAR NOT NULL);');
	create_table('idade', 'CREATE TABLE IF NOT EXISTS idade(id_idade INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, idade VARCHAR NOT NULL);');
	create_table('religiao', 'CREATE TABLE IF NOT EXISTS religiao(id_religiao INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, religiao VARCHAR NOT NULL);');
	create_table('pergunta', 'CREATE TABLE IF NOT EXISTS pergunta(id_pergunta INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_pesquisa INTEGER NOT NULL, pergunta VARCHAR NOT NULL, multi INTEGER NOT NULL, obrigatorio INTEGER NOT NULL);');
	create_table('pesquisa', 'CREATE TABLE IF NOT EXISTS pesquisa(id_pesquisa INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_cliente INTEGER NOT NULL, nome VARCHAR NOT NULL);');
	create_table('resposta', 'CREATE TABLE IF NOT EXISTS resposta(id_resposta INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_pergunta INTEGER NOT NULL, resposta VARCHAR NOT NULL);');
	create_table('sexo', 'CREATE TABLE IF NOT EXISTS sexo(id_sexo INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, sexo VARCHAR NOT NULL);');
	create_table('regiao', 'CREATE TABLE IF NOT EXISTS regiao(id_regiao INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, regiao VARCHAR NOT NULL);');
	create_table('entrevista', 'CREATE TABLE IF NOT EXISTS entrevista(id_entrevista INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_pesquisa INTEGER NOT NULL, id_atendente INTEGER NOT NULL, id_sexo INTEGER NOT NULL, id_idade INTEGER NOT NULL, id_escolaridade INTEGER NOT NULL, id_classe INTEGER NOT NULL, id_regiao INTEGER NOT NULL, id_religiao INTEGER NOT NULL);');
	create_table('entrevista_resultado', 'CREATE TABLE IF NOT EXISTS entrevista_resultado(id_entrevista_resultado INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, id_entrevista INTEGER NOT NULL, id_pesquisa INTEGER NOT NULL, id_atendente INTEGER NOT NULL, id_pergunta INTEGER NOT NULL, id_resposta INTEGER NOT NULL);');
}

function limpar_dados(){
	$("#lista_log").children().remove();
	delete_data('atendente');
	delete_data('classe');
	delete_data('cliente');
	delete_data('escolaridade');
	delete_data('idade');
	delete_data('religiao');
	delete_data('pergunta');
	delete_data('pesquisa');
	delete_data('resposta');
	delete_data('sexo');
	delete_data('regiao');
}

function carregar_dados(){
	$("#lista_log").children().remove();
	insert_data('atendente');
	insert_data('classe');
	insert_data('cliente');
	insert_data('escolaridade');
	insert_data('idade');
	insert_data('religiao');
	insert_data('pergunta');
	insert_data('pesquisa');
	insert_data('resposta');
	insert_data('sexo');
	insert_data('regiao');
}


function clearStatus(){
	$("#lista_log").html('');
}

function updateStatus(msg,st){
	console.log(msg+' - '+st);
	//$('#lista_log').bind('pageinit', function() {
		//$("#lista_log").append('<li data-icon="arrow-r"><a href="#"><span>'+msg+'</span><br>	<span class="titulo_cliente">'+st+'</span></a>			</li>').listview("refresh");
		$("#lista_log").append('<li data-icon="arrow-r"><a href="#"><span>'+msg+'</span><br>	<span class="titulo_cliente">'+st+'</span></a>			</li>');
	//  });
}




nullDataHandler = function(transaction, results){
}
errorHandler = function(transaction, error){
	alert(error.message);
    //updateStatus("Erro: ", error.message);
    return true;
}



var pesquisador = 0;

function logar(){
	var login = $('#login').val();
	var senha = $('#senha').val();
	
	if(login == "" || senha == ""){
		alert("Informe o Login e Senha");
	} else if(login == "admin" && senha == "voxdata"){
		irPara("page_admin");
	} else {
		var query = "select count(*) as total from atendente where login = '"+login+"' and senha = '"+senha+"'";
		try {
			localDB.transaction(function(transaction){
        
				transaction.executeSql(query, [], function(transaction, results){
					var row = results.rows.item(0);
					console.log(localDB);
					console.log(query);
					if(row['total']){
						carregaLogin(login, senha)
					} else {
						alert("Login Inválido");
					}
					
				}, function(transaction, error){
					updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
				});
			});
		} 
		catch (e) {
			//updateStatus("Erro: Tabela não excluida " + e + ".");
			return;
		}
	}
}

function carregaLogin(login, senha){

	var query = "select * from atendente where login = '"+login+"' and senha = '"+senha+"'";
	try {
		localDB.transaction(function(transaction){
	
			transaction.executeSql(query, [], function(transaction, results){
				var row = results.rows.item(0);
				pesquisador = row['id_atendente'];
				carregar_pesquisa();
				irPara("page_menu");
				
			}, function(transaction, error){
				updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
			});
		});
	} 
	catch (e) {
		//updateStatus("Erro: Tabela não excluida " + e + ".");
		return;
	}
	
}


function sem_conexao(){
	tentativa_login = 1;
	$("#botao_login").html('<span class="ui-btn-inner"><span class="ui-btn-text">FAZER LOGIN</span></span>');
	alerta("Sem conexão com a Internet!</b><br />Verifique sua conexão e tente novamente.");
	alert("Sem conexão com a Internet!");
}


function irPara(pg){
	$.mobile.changePage('#'+pg, {transition: 'flip'});
}

function alerta(msg){
	$("#mensagem").html(msg);
	$('#caixa_mensagem').fadeIn('fast'); 
}

function fecharAlerta(){
	$('#caixa_mensagem').fadeOut('fast'); 
}



function carregar_pesquisa(){
	var query = "SELECT * FROM pesquisa inner join cliente on pesquisa.id_cliente =  cliente.id_cliente;";
	console.log(query);
    try {
        localDB.transaction(function(transaction){
        
            transaction.executeSql(query, [], function(transaction, results){
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);

					str = '<li data-icon="arrow-r"><a href="javascript:iniciar_pesquisa('+row['id_pesquisa']+')">				<span>'+row['nome']+'</span><br>				<span class="titulo_cliente">'+row['nome_razao']+'</span></a>			</li>';
					$("#lista_pesquisa").append(str).listview("refresh");
                }
            }, function(transaction, error){
                updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
            });
        });
    } 
    catch (e) {
        updateStatus("Error: SELECT não realizado " + e + ".");
    }
}

function carregar_sexo(){
	var query = "SELECT * FROM sexo;";
    try {
        localDB.transaction(function(transaction){
        
            transaction.executeSql(query, [], function(transaction, results){
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
					$("#sexo").append(new Option(row['sexo'], row['id_sexo']));

					$('#sexo').selectmenu();
					$('#sexo').selectmenu('refresh', true);
                }
            }, function(transaction, error){
                updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
            });
        });
    } 
    catch (e) {
        updateStatus("Error: SELECT não realizado " + e + ".");
    }
}

function carregar_regiao(){
	var query = "SELECT * FROM regiao;";
    try {
        localDB.transaction(function(transaction){
        
            transaction.executeSql(query, [], function(transaction, results){
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
					$("#regiao").append(new Option(row['regiao'], row['id_regiao']));

					$('#regiao').selectmenu();
					$('#regiao').selectmenu('refresh', true);
                }
            }, function(transaction, error){
                updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
            });
        });
    } 
    catch (e) {
        updateStatus("Error: SELECT não realizado " + e + ".");
    }
}

function carregar_idade(){
	var query = "SELECT * FROM idade;";
    try {
        localDB.transaction(function(transaction){
        
            transaction.executeSql(query, [], function(transaction, results){
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
					$("#idade").append(new Option(row['idade'], row['id_idade']));

					$('#idade').selectmenu();
					$('#idade').selectmenu('refresh', true);
                }
            }, function(transaction, error){
                updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
            });
        });
    } 
    catch (e) {
        updateStatus("Error: SELECT não realizado " + e + ".");
    }
}

function carregar_religiao(){
	var query = "SELECT * FROM religiao;";
    try {
        localDB.transaction(function(transaction){
        
            transaction.executeSql(query, [], function(transaction, results){
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
					$("#religiao").append(new Option(row['religiao'], row['id_religiao']));

					$('#religiao').selectmenu();
					$('#religiao').selectmenu('refresh', true);
                }
            }, function(transaction, error){
                updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
            });
        });
    } 
    catch (e) {
        updateStatus("Error: SELECT não realizado " + e + ".");
    }
}

function carregar_escolaridade(){

	var query = "SELECT * FROM escolaridade;";
    try {
        localDB.transaction(function(transaction){
        
            transaction.executeSql(query, [], function(transaction, results){
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
					$("#escolaridade").append(new Option(row['escolaridade'], row['id_escolaridade']));

					$('#escolaridade').selectmenu();
					$('#escolaridade').selectmenu('refresh', true);
                }
            }, function(transaction, error){
                updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
            });
        });
    } 
    catch (e) {
        updateStatus("Error: SELECT não realizado " + e + ".");
    }
}
function carregar_classe(){
	
	var query = "SELECT * FROM classe;";
    try {
        localDB.transaction(function(transaction){
        
            transaction.executeSql(query, [], function(transaction, results){
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
					$("#classe").append(new Option(row['classe'], row['id_classe']));

					$('#classe').selectmenu();
					$('#classe').selectmenu('refresh', true);
                }
            }, function(transaction, error){
                updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
            });
        });
    } 
    catch (e) {
        updateStatus("Error: SELECT não realizado " + e + ".");
    }

	
}
var arrayPergunta = new Array;
var seqPergunta = new Array;
var arrayResposta = new Array;
var contadorPergunta = 0;

function proximaPergunta(){
	
	if(arrayPergunta.length == contadorPergunta){
		sexo = $("#sexo").val();
		idade = $("#idade").val();
		escolaridade = $("#escolaridade").val();
		classe = $("#classe").val();
		regiao = $("#regiao").val();
		religiao = $("#religiao").val()
		
		var query = 'insert into entrevista (id_entrevista, id_pesquisa, id_atendente, id_sexo, id_idade, id_escolaridade, id_classe, id_regiao, id_religiao) values (?,?,?,?,?,?,?,?,?);';

		try {
			localDB.transaction(function(transaction){
				transaction.executeSql(query, [id_entrevista, pesquisa, pesquisador, sexo, idade, escolaridade, classe, regiao, religiao], nullDataHandler, errorHandler);
				cadastra_resposta(id_entrevista);
			});
		} 
		catch (e) {
			updateStatus(tabela, e);
			return;
		}
	}
	var chk = $("#page_pesquisa"+arrayPergunta[contadorPergunta-1]+" .radio-choice:checked").length;
	//alert($("#page_pesquisa"+contadorPergunta).length);
	if(contadorPergunta > 0){
		if(chk > 0 || $("#page_pesquisa"+arrayPergunta[contadorPergunta-1]).attr("required") != "required"){
			irPara('page_pesquisa'+arrayPergunta[contadorPergunta]);
			contadorPergunta++;
		} else {
			//alert($("#page_pesquisa"+arrayPergunta[contadorPergunta-1]).attr("required"));
			alert("Selecione uma opção");
		}
	} else {
		if($("#sexo").val() == 0) alert("Selecione o Sexo para continuar");
		else if($("#idade").val() == 0) alert("Selecione a Idade para continuar");
		else if($("#escolaridade").val() == 0) alert("Selecione a Escolaridade para continuar");
		else if($("#classe").val() == 0) alert("Selecione a Classe para continuar")
		else if($("#regiao").val() == 0) alert("Selecione a Região para continuar")
		else if($("#religiao").val() == 0) alert("Selecione a Região para continuar")
		else {
			irPara('page_pesquisa'+arrayPergunta[contadorPergunta]);
			contadorPergunta++;
		}
	}
	
	/*
	if(contadorPergunta > 0)
		if(chk > 0){
			irPara('page_pesquisa'+arrayPergunta[contadorPergunta]);
			contadorPergunta++;
		} else {
			alert("Selecione uma opção");
	} else {
		if($("#sexo").val() == 0) alert("Selecione o Sexo para continuar");
		else if($("#idade").val() == 0) alert("Selecione a Idade para continuar");
		else if($("#escolaridade").val() == 0) alert("Selecione a Escolaridade para continuar");
		else if($("#classe").val() == 0) alert("Selecione a Classe para continuar")
		else {
			irPara('page_pesquisa'+arrayPergunta[contadorPergunta]);
			contadorPergunta++;
		}
	}*/
	
	
}


function cadastra_resposta(id){
	
	try {
		localDB.transaction(function(transaction){
			
			query = 'insert into entrevista_resultado (id_entrevista, id_pesquisa, id_atendente, id_pergunta, id_resposta) values (?,?,?,?,?);';
			
			$("input[type=radio]:checked").each(function(index, element) {
				pergunta = $("#"+this.id).attr("pergunta");
				resposta = $("#"+this.id).attr("resposta");
				transaction.executeSql(query, [id, pesquisa, pesquisador, pergunta, resposta], nullDataHandler, errorHandler);
			});
		
			$("input[type=checkbox]:checked").each(function(index, element) {
				pergunta = $("#"+this.id).attr("pergunta");
				resposta = $("#"+this.id).attr("resposta");
				transaction.executeSql(query, [id, pesquisa, pesquisador, pergunta, resposta], nullDataHandler, errorHandler);
			});
			
			$("input[type=checkbox]").attr("checked",false).checkboxradio("refresh");
			$("input[type=radio]").attr("checked",false).checkboxradio("refresh");
			
			contadorPergunta = 0;
			
			irPara("page_menu");
		});
	} 
	catch (e) {
		
		updateStatus(tabela, e);
		return;
	}

}




function sincronizar(){
	var query = "SELECT * FROM entrevista";
	
    try {
        localDB.transaction(function(transaction){
        
            transaction.executeSql(query, [], function(transaction, results){
				sql = "";
			    for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
					sql += row['id_entrevista']+","+row['id_pesquisa']+","+row['id_atendente']+","+row['id_sexo']+","+row['id_idade']+","+row['id_escolaridade']+","+row['id_classe']+","+row['id_regiao']+","+row['id_religiao']+"-";
                }
				$.post(raiz+"ajax.php?funcao=sincronizar", {sql: sql},
					function(data) {
						if(data){
							//alert(data);
							//delete_data('entrevista');
						}
						sincronizar_entrevista_resultado();
					}
				)
				
            }, function(transaction, error){
                updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
            });
        });
    } 
    catch (e) {
        updateStatus("Error: SELECT não realizado " + e + ".");
    }

	//irPara("page_perfil");
}

function sincronizar_entrevista_resultado(){
	var query = "SELECT * FROM entrevista_resultado";
	
    try {
        localDB.transaction(function(transaction){
        
            transaction.executeSql(query, [], function(transaction, results){
				sql = "";
			    for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
					sql += row['id_entrevista_resultado']+","+row['id_entrevista']+","+row['id_pesquisa']+","+row['id_atendente']+","+row['id_pergunta']+","+row['id_resposta']+"-";
                }
				//alert(sql);
				console.log(sql);
				$.post(raiz+"ajax.php?funcao=sincronizar_entrevista_resultado", {sql: sql},
					function(data) {
						if(data){
							//delete_data('entrevista_resultado');
						}
					}
				)
				
            }, function(transaction, error){
                updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
            });
        });
    } 
    catch (e) {
        updateStatus("Error: SELECT não realizado " + e + ".");
    }

	//irPara("page_perfil");
}

function carregar_pergunta(){
	$(".paginas").remove();
	
	var query = "SELECT * FROM pergunta where id_pesquisa = "+pesquisa;
	
    try {
        localDB.transaction(function(transaction){
        
            transaction.executeSql(query, [], function(transaction, results){
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
					arrayPergunta[i] = row['id_pergunta'];
					
					obr = '';
					if(row['obrigatorio'] == 1){
						obr = 'required="required"';
					}
					
					div = '<div id="page_pesquisa'+row['id_pergunta']+'" '+obr+' class="paginas" data-role="page" data-theme="a"><div data-dividertheme="a" data-role="listview" data-inset="false">';
					per = '<li class="titulo_painel" style="font-size:24px !important" data-role="list-divider">'+row['pergunta']+'</li>';
					rad = '<li id="lista_resposta" data-icon="arrow-r"><fieldset  data-role="controlgroup">'
					pagina = div+per+rad;
					seqPergunta[i] = row['id_pergunta'];
					carregar_resposta(row['id_pergunta'], pagina, row['multi']);
                }
            }, function(transaction, error){
                updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
            });
        });
    } 
    catch (e) {
        updateStatus("Error: SELECT não realizado " + e + ".");
    }
	//alert($(".paginas").length);
	irPara("page_perfil");
}

function setar_resposta(p,r){
	arrayResposta[p] = r;
	
	/*$("input[type=radio]:checked").each(function(index, element) {
		alert($("#"+this.id).attr("pergunta"));
		alert($("#"+this.id).attr("resposta"));
	});

	$("input[type=checkbox]:checked").each(function(index, element) {
		alert($("#"+this.id).attr("pergunta"));
		alert($("#"+this.id).attr("resposta"));
	});*/
	
	//$("input[type=checkbox]:checked").attr('checked',false);
	
	
	
}

function carregar_resposta(pergunta,div,multi){
	var query = "SELECT * FROM resposta where id_pergunta = "+pergunta;
   
	
	//alert(query);
	try {
        localDB.transaction(function(transaction){
        respostas = "";
            transaction.executeSql(query, [], function(transaction, results){
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
					
					if(multi == 1){
						tipo = "checkbox";
					} else {
						tipo = "radio";
					}
					
					respostas += '<input onclick="setar_resposta('+pergunta+','+row['id_resposta']+')" type="'+tipo+'"  class="radio-choice" name="radio-choice'+row['id_pergunta']+'" id="radio-choice-'+row['id_resposta']+'" pergunta="'+row['id_pergunta']+'" resposta="'+row['id_resposta']+'" value="resposta-'+row['id_pergunta']+'-'+row['id_resposta']+'"><label for="radio-choice-'+row['id_resposta']+'">'+row['resposta']+'</label>';
					//alert(row['resposta']);
                }
				div2 = '</fieldset></li><li data-icon="arrow-r"> <div onClick="proximaPergunta();" data-icon="arrow-r" data-theme="b" data-role="button" type="">Próxima Pergunta</div> </li></div></div>';
				pagina = div+respostas+div2;
				//alert(pagina);
				$('body').append(pagina);
            }, function(transaction, error){
                updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
            });
        });
    } 
    catch (e) {
        updateStatus("Error: SELECT não realizado " + e + ".");
    }
	//alert(respostas);
	//return respostas;
}
var perfilCarregado = 0;
function iniciar_pesquisa(p){
	var d = new Date();
	id_entrevista = d.getTime();
	pesquisa = p;
	contadorPergunta = 0;
	if(perfilCarregado == 0){
		carregar_sexo();
		carregar_regiao();
		carregar_idade();
		carregar_religiao();
		carregar_escolaridade();
		carregar_classe();
		perfilCarregado = 1;
	}
	
	carregar_pergunta();
}

function estatistica(){
	
	var query = "SELECT count(*) as total FROM entrevista;";
    try {
        localDB.transaction(function(transaction){
        
            transaction.executeSql(query, [], function(transaction, results){
                for (var i = 0; i < results.rows.length; i++) {
                    var row = results.rows.item(i);
					$("#qtd_pesquisa").html("Pesquisas Realizadas: "+row['total']);
                }
            }, function(transaction, error){
                updateStatus("Erro: " + error.code + "<br>Mensagem: " + error.message);
            });
        });
    } 
    catch (e) {
        updateStatus("Error: SELECT não realizado " + e + ".");
    }
	
	irPara("page_estatistica");
	
	
}