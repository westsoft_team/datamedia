<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
<title>Voxdata - Relatório</title>
<style type="text/css">
.r { text-align: right; }

table tr td { border:solid 1px #333; }

table{
	border-collapse:collapse;
}

.grafico{

}

*{
	font-family:Arial, Helvetica, sans-serif;
}
</style>
</head>
<?
	include_once 'conecta.php';
	
	include_once 'phplot-5.8.0/phplot.php';
	
	
?>


<body>


<?
	$pesquisa = 62;
	
	$sql = "select * from pesquisa where id_pesquisa = $pesquisa";
	$res = mysql_query($sql);
	$row = mysql_fetch_assoc($res);
	$titulo_pesquisa = $row['descricao'];
	
	$sql = "select a.id_entrevista, a.id_pesquisa, a.id_atendente, a.id_pergunta, a.id_resposta
			from entrevista_resultado a
			inner join entrevista using (id_entrevista)
			where a.id_pesquisa = $pesquisa
			group by a.id_entrevista, a.id_pesquisa
			order by a.id_entrevista, a.id_pesquisa, a.id_atendente, a.id_pergunta";
	$res = mysql_query($sql);
	$total_pesquisas = mysql_num_rows($res);

?>


<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<th align="left" scope="col"><h1><?=$titulo_pesquisa?></h1></th>
		<th rowspan="2" scope="col"><img width="200" src="css/vox.png" /></th>
	</tr>
	<tr>
		<th scope="col">
			<table  bordercolor="#CCCCCC" border="0" cellspacing="0" cellpadding="8">
				<tr>
					<td width="150" bgcolor="#CCCCCC" class="r">Pesquisas realizadas</td>
					<td colspan="2" bgcolor="#CCCCCC"><strong><?=$total_pesquisas?></strong></td>
				</tr>
			</table>
		</th>
	</tr>
</table>

<br clear="all" />


<?
	$sql = "select * from pergunta where id_pesquisa = $pesquisa";
	$res = mysql_query($sql);
	while($row = mysql_fetch_assoc($res)){
		
		$id_pergunta = $row['id_pergunta'];
?>
<br clear="all" /><br clear="all" />
<fieldset style="background-color:#F5F5F5">
<h2><?=$row['pergunta']?></h2>
<table border="0" width="100%" cellspacing="0" cellpadding="8">
	<tr>
		<td  align="left" bgcolor="#CCCCCC"><strong>Geral</strong></td>
		<td width="130" align="center" bgcolor="#CCCCCC">%</td>
		<td width="130" align="center" bgcolor="#CCCCCC">Respostas</td>
	</tr>
<?
	$sql2 = "select * from resposta where id_pergunta = $id_pergunta";
	$res2 = mysql_query($sql2);
	$porcentagemTotal = 0;
	$votosTotal = 0;
	$i=1;
	while($row2 = mysql_fetch_assoc($res2)){
		$totalResposta = mysql_num_rows($res2);
		$id_resposta = $row2['id_resposta'];
		$sql3 = "select a.id_entrevista, a.id_pesquisa, a.id_atendente, a.id_pergunta, a.id_resposta
				from entrevista_resultado a
				inner join entrevista using (id_entrevista)
				where a.id_pesquisa = $pesquisa
				and a.id_pergunta = $id_pergunta
				and a.id_resposta = $id_resposta
				group by a.id_entrevista, a.id_pesquisa, a.id_atendente, a.id_pergunta
				order by a.id_entrevista, a.id_pesquisa, a.id_atendente, a.id_pergunta";

		$res3 = mysql_query($sql3);
		$total = mysql_num_rows($res3);
		
		$porcentagem = round($total / $total_pesquisas * 100,1);
		
		if($totalResposta == $i){
			$porcentagem = round(100 - $porcentagemTotal,1);
			$total = $total_pesquisas - $votosTotal;
		}
		$i++;
		
		$votosTotal += $total;
		$porcentagemTotal += $porcentagem;
		//$milliseconds =  $row3['id_entrevista'];
		//$timestamp = $milliseconds/1000;
		//echo date("d/m/Y H:m:i-Z", $timestamp);
?>    
		<tr>
			<td align="" bgcolor="#E1E1E1"><?=$row2['resposta']?></td>
			<td align="center" bgcolor="#FFFFFF" align=""><?= $porcentagem."%"?></td>
			<td bgcolor="#FFFFFF" align="center"><?= $total?></td>
		</tr>
<?
	}
?>        
	<tr bgcolor="#6C6C6C" style="color:#fff">
		<td align="left" >Total</td>
		<td  align="center"><?= $porcentagemTotal."%"?></td>
		<td  align="center"><?= $votosTotal?></td>
	</tr>
    
        
	</tr>    

</table>
<br clear="all" /><br clear="all" />

<?
	$arr[0]['tabela'] = 'sexo';
	$arr[0]['titulo'] = 'SEXO';
	
	$arr[1]['tabela'] = 'idade';
	$arr[1]['titulo'] = 'IDADE';
	
	$arr[2]['tabela'] = 'escolaridade';
	$arr[2]['titulo'] = 'ESCOLARIDADE';
	
	$arr[3]['tabela'] = 'classe';
	$arr[3]['titulo'] = 'CLASSE SOCIAL';
	
	for($i=0; $i<count($arr); $i++){
	
?>


<table border="0" width="100%" cellspacing="0" cellpadding="8">
	<tr>
	
		<td  align="left" bgcolor="#CCCCCC"><strong><?=$arr[$i]['titulo']?></strong></td>
<?
	$sql4 = "select * from ".$arr[$i]['tabela'];
	$res4 = mysql_query($sql4);
	while($row4 = mysql_fetch_assoc($res4)){
		$id_resposta = $row4['id_resposta'];
	
?>			
			<td width="130" bgcolor="#CCCCCC" align="center"><?= $row4[$arr[$i]['tabela']]?></td>
<?
	}
?>			
	</tr>
<?
	$sql2 = "select * from resposta where id_pergunta = $id_pergunta";
	$res2 = mysql_query($sql2);
	$porcentagemTotal = 0;
	$votosTotal = 0;
	
	while($row2 = mysql_fetch_assoc($res2)){
		
?>    
		<tr>
			<td align="" bgcolor="#E1E1E1"><?=$row2['resposta']?></td>
<?
	$sql5 = "select * from ".$arr[$i]['tabela'];
	$res5 = mysql_query($sql5);
	$porcentagemTotal = 0;
	$votosTotal = 0;
	
	while($row5 = mysql_fetch_assoc($res5)){
		$id_resposta = $row2['id_resposta'];
		
		$id_sexo = $row5['id_'.$arr[$i]['tabela']];
		
		$sql3 = "select a.id_entrevista, a.id_pesquisa, a.id_atendente, a.id_pergunta, a.id_resposta
				from entrevista_resultado a
				inner join entrevista using (id_entrevista)
				where a.id_pesquisa = $pesquisa
				and id_".$arr[$i]['tabela']." = $id_sexo
				and a.id_pergunta = $id_pergunta
				
				order by a.id_entrevista, a.id_pesquisa, a.id_atendente, a.id_pergunta";
		$res3 = mysql_query($sql3);
		$row3 = mysql_fetch_assoc($res3);
		$totalPergunta = mysql_num_rows($res3);

		
		$sql3 = "select a.id_entrevista, a.id_pesquisa, a.id_atendente, a.id_pergunta, a.id_resposta
				from entrevista_resultado a
				inner join entrevista using (id_entrevista)
				where a.id_pesquisa = $pesquisa
				and id_".$arr[$i]['tabela']." = $id_sexo
				and a.id_resposta = $id_resposta
				group by a.id_entrevista, a.id_pesquisa, a.id_atendente, a.id_pergunta
				order by a.id_entrevista, a.id_pesquisa, a.id_atendente, a.id_pergunta";
		$res3 = mysql_query($sql3);
		$row3 = mysql_fetch_assoc($res3);
	
		$total = mysql_num_rows($res3);
		
		 $porcentagem = round($total / $totalPergunta * 100,1);

		$porcentagemTotal += $porcentagem;
	
?>			
			<td bgcolor="#FFFFFF" align="center"><?= $porcentagem."%"?></td>
<?
	}
?>			
			
		</tr>
<?
	}
?>        

	</tr>    
</table>
<br clear="all" /><br clear="all" />
<?
	}
?>

</fieldset>


<?
	}
?>

</body>
</html>
