<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
<title>Voxdata - Relatório</title>
<style type="text/css">
.r { text-align: right; }

table tr td { border:solid 1px #333; }

table{
	border-collapse:collapse;
}

.grafico{

}

* {
	font-family:Arial, Helvetica, sans-serif;
	font-size:12px;
}
</style>
</head>
<?
	include_once 'conecta.php';
	
	include_once 'phplot-5.8.0/phplot.php';
	
?>



<body>

<form action="" method="post">
	<select name="pesquisa">
<?
	$sql = "select * from pesquisa";
	$res = mysql_query($sql);
	while($row = mysql_fetch_assoc($res)){
		$id_pesquisa = $row['id_pesquisa'];
		$pesquisa = $row['descricao'];
		$chk = $_POST['pesquisa'] == $id_pesquisa ? " selected " : "";
		echo "<option $chk value='$id_pesquisa'>$pesquisa</option>";

	}
?>		
	</select>
	<input type="submit" value="Exibir" />
</form>

<?
	if($_POST){
		$id_pesquisa = $_POST['pesquisa'];
		$sql = "select * from entrevista where id_pesquisa = $id_pesquisa";
		$res = mysql_query($sql);
		echo $total = mysql_num_rows($res);
	}
?>

<table bordercolor="#CCCCCC" border="0" cellspacing="0" cellpadding="8">
	<tr>
		<td width="100" bgcolor="#CCCCCC" class="r">Pesquisas realizadas</td>
		<td colspan="2" bgcolor="#CCCCCC"><strong><?=$total?></strong></td>
	</tr>
</table>
<br clear="all" />
<?
	$sql = "select * from pergunta where id_pesquisa = $id_pesquisa";
	$res = mysql_query($sql);
	while($row = mysql_fetch_assoc($res)){
		$id_pergunta = $row['id_pergunta'];
?>
<table border="0" width="600" cellspacing="0" cellpadding="8">
	<tr>
		<td colspan="8" align="left" bgcolor="#CCCCCC"><strong><?=$row['pergunta']?></strong></td>
	</tr>
	<tr>
<?
	$sql2 = "select * from resposta where id_pergunta = $id_pergunta";
	$res2 = mysql_query($sql2);
	while($row2 = mysql_fetch_assoc($res2)){
		
?>    
		<td align="center" bgcolor="#E1E1E1"><?=$row2['resposta']?></td>
<?
	}
?>        
	</tr>
    
	<tr>
<?
	$res2 = mysql_query($sql2);
	while($row2 = mysql_fetch_assoc($res2)){
		$id_resposta = $row2['id_resposta'];
		echo $sql3 = "select count(*) as total from entrevista_resultado where id_pergunta = $id_pergunta and id_resposta = $id_resposta group by id_entrevista";
		$res3 = mysql_query($sql3);
		$row3 = mysql_fetch_assoc($res3);
?>    
		<td align="center" bgcolor="#ffffff"><?=$row3['total']?></td>
<?
	}
?>        
	</tr>    


	<tr>
<?
	$res2 = mysql_query($sql2);
	while($row2 = mysql_fetch_assoc($res2)){
		$id_resposta = $row2['id_resposta'];
		$sql3 = "select count(*) as total from entrevista_resultado where id_pergunta = $id_pergunta and id_resposta = $id_resposta group by id_entrevista";
		$res3 = mysql_query($sql3);
		$row3 = mysql_fetch_assoc($res3);
?>    
		<td align="center" bgcolor="#ffffff"><?= number_format($row3['total'] / $total * 100,2)."%"?></td>
<?
	}
?>        
	</tr>
	

</table>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
	  
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Dia'
<?
	$res2 = mysql_query($sql2);
	while($row2 = mysql_fetch_assoc($res2)){
		echo ",'".$row2['resposta']."'";
	}
?>
		  ],
		  
<?
	$sql4 = "select *, date(data_resultado) as dt from resultado group by date(data_resultado)";
	$res4 = mysql_query($sql4);
	while($row4 = mysql_fetch_assoc($res4)){
		$dt = $row4['dt'];
		$dt2 = converterData($row4['dt']);
		
		echo "['$dt2'";
		
		$sql5 = "select * from pergunta_resposta
				left join resposta using (id_resposta)
				where id_pergunta = $id_pergunta";
		$res5 = mysql_query($sql5);
		while($row5 = mysql_fetch_assoc($res5)){
			$id_resp = $row5['id_resposta'];
			
			$sql6 = "select count(*) as total from resposta
					left join resultado_item using (id_resposta)
					left join resultado using (id_resultado)
					where id_pergunta = $id_pergunta
					and date(data_resultado) = '$dt'";
			$res6 = mysql_query($sql6);
			$row6 = mysql_fetch_assoc($res6);
			
			$tt = $row6['total'];	
			
			$sql6 = "select count(*) as total from resposta
					left join resultado_item using (id_resposta)
					left join resultado using (id_resultado)
					where id_resposta = $id_resp
					and id_pergunta = $id_pergunta
					and date(data_resultado) = '$dt'";
				$res6 = mysql_query($sql6);
				while($row6 = mysql_fetch_assoc($res6)){
					echo ",".number_format($row6['total'] / $tt * 100,2);
				}
			
		}
?>		  
          ],
<?
	}
?>		  
        ]);

        var options = {
          title: '<?=$row['pergunta']?>',
		  backgroundColor: '#fff',
		  lineWidth: 4
        };

        var chart<?=$id_pergunta?> = new google.visualization.AreaChart(document.getElementById('chart_div<?=$id_pergunta?>'));
        chart<?=$id_pergunta?>.draw(data, options);
      }
    </script>
<div id="chart_div<?=$id_pergunta?>" class="grafico" style="width:900px; height: 500px;"></div>
<br clear="all" /><br clear="all" />
<?
	}
	
	function converterData($data){
	if (strstr($data, "/")){
		$A = explode ("/", $data);
		$V_data = $A[2] . "-". $A[1] . "-" . $A[0];
	} else {
		$A = explode ("-", $data);
		$V_data = $A[2] . "/". $A[1] . "/" . $A[0];
	}
	return $V_data;
}
	
?>

</body>
</html>
