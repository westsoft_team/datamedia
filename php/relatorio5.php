<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
<link href="css/screen.css" rel="stylesheet" type="text/css" media="screen" />
<link href="css/print.css" rel="stylesheet" type="text/css" media="print" />
<title>Voxdata - Relat�rio</title>
<style type="text/css">
.r { text-align: right; }

table tr td { border:solid 1px #333; }

table{
	border-collapse:collapse;
}

.grafico{

}

*{
	font-family:Arial, Helvetica, sans-serif;
}
</style>
</head>
<?
	include_once 'conecta.php';
	
	//print_r($_POST);
	
	$arr[0]['tabela'] = 'sexo';
	$arr[0]['titulo'] = 'SEXO';
	
	$arr[1]['tabela'] = 'idade';
	$arr[1]['titulo'] = 'IDADE';
	
	$arr[2]['tabela'] = 'escolaridade';
	$arr[2]['titulo'] = 'ESCOLARIDADE';
	
	$arr[3]['tabela'] = 'classe';
	$arr[3]['titulo'] = 'CLASSE SOCIAL';
	
	$arr[4]['tabela'] = 'regiao';
	$arr[4]['titulo'] = 'REGIAO';
	
	$radio = "";
	if(isset($_POST)){
		if(count($_POST['id_resposta'])){
			$radio .= " and (";
			for($i=0; $i<count($_POST['id_resposta']); $i++){
				$aux = explode("radio_", $_POST['id_resposta'][$i]);
				$radio .= " id_resposta = ".end($aux);
				$radio .= $i < count($_POST['id_resposta']) -1 ? " or " : "";
			}
			$radio .= ")";
		}
	}
	//echo $radio;
	
	$filtros = 0;
	$where = "";
	if(isset($_POST)){
		if(count($_POST['id_resposta'])){
			$where .= " and (";
			for($i=0; $i<count($_POST['id_resposta']); $i++){
				if(!strstr($_POST['id_resposta'][$i], "radio_")){
					$where .= " id_resposta = ".$_POST['id_resposta'][$i];
					$where .= $i < count($_POST['id_resposta']) -1 ? " or " : "";
					$filtros++;
				}	
			}
			$where .= ") ";
		}
	}
	$where = $filtros ? $where : "";
	
	$perfil = 0;
	$comp = "";
	if(isset($_POST)){
		for($i=0; $i<count($arr); $i++){
			$tabela = $arr[$i]['tabela'];
			if(count($_POST["$tabela"])){
				$comp .= " and (";
				for($j=0; $j<count($_POST["$tabela"]); $j++){
					$comp .= " id_$tabela = ".$_POST["$tabela"][$j];
					$comp .= $j < count($_POST["$tabela"]) -1 ? " or " : "";
					$perfil++;
				}
				$comp .= ") ";
			}
		}
	}
	
	//echo $comp;
	
?>


<body>

<div id="metodologia">
<img src="css/vox.png" />
<h1 style="text-align:center">METODOLOGIA</h1>
<?
	echo nl2br("
	
Pesquisa Voxdata realizada no munic�pio de Cascavel/Paran�, com o objetivo de consulta a popula�ao sobre avalia�ao de n�vel de audiencia de r�dio, al�m de avalia�ao especifica sobre a qualidade do servi�o prestado. Foi entrevistada uma pessoa por residencia ou grupo, sendo este por abordagem feita em car�ter aleat�rio. Serviu como base para a estratifica�ao das entrevistas o IBGE.

A identifica�ao pessoal foi tomada pelo coletor atrav�s de abordagem, sendo que as avalia�oes e inten�ao de escolha foi obtida atrav�s de resposta estimulada.

As amostras foram divididas nos seguintes extratos; sexo, idade, classe social, escolaridade e regiao al�m da estratifica�ao feita por hor�rios, dias da semana e locais, sendo assim as amostras foram divididas entre 31 bairros e distritos da cidade, pesquisados de maneira proporcional ao peso da popula�ao de cada um dos mesmos, o mesmo foi feito para os demais extratos, sexo, idade, classe social e escolaridade.  
	
Foram ouvidas dos dias 26 de Setembro � 02 de Outubro de 2013, 660 pessoas nas seguintes regioes:
	<strong>REGIAO NORTE</strong>: INTERLAGOS (Interlagos, Taruma e Abelha), BRASMADEIRA (Brasmadeira, Tocantins 1, Tocantins 2, Rio Branco, Jardim Paran�, Garbim e Jardim Cuiab�), FLORESTA (Floresta, Clarito), BRASILIA (Alvorada, Consolata, Verdes Campos, Brasilia 1 e Brasilia 2), MORUMBI (Morumbi, Lago Azul), PERIOLO (Periolo, Jardim Belo Horizonte, Conjunto Sao Francisco e Ipanema).
	<strong>REGIAO SUL</strong>: UNIVERSIT�RIO (Universit�rio, Sao Luiz, Parque Cascavel, Turisparque, Santa Catarina, Imperial e Panor�mico), SANTA FELICIDADE (Santa Felicidade, Petr�polis e Jardim Horizonte), GUARUJ� (Guaruj�, Indianara Ghislandi), XIV DE NOVEMBRO (XIV de Novembro, Esplanada, Jardim Montreal e Quebec), PARQUE SAO PAULO (Sao Paulo), PIONEIROS CATARINENSE (Pioneiros Catarinenses e Ninho da Cobra), FACULDADE, JARDIM UNIAO.
	<strong>REGIAO LESTE</strong>: CASCAVEL VELHO (Cascavel Velho, Nova It�lia, Presidente e Jardim Veneza), SAO CRISTOVAO (Sao Cristovao, Loteamento Pinheiros, Jardim Aparecida, Jardim Fran�a, Primavera, Vila Aparecida), JARDIM GRAMADO, PACAEMBU (Pacaembu e Nacional), REGIAO DO LAGO (Caravelle, Itamarati e Nova York), CATARATAS (Cataratas, Loteamento Sao Francisco, Colm�ia e Morada do Sol).
	<strong>REGIAO OESTE</strong>: CANCELLI, ALTO ALEGRE, COQUEIRAL (Coqueiral e Aclima�ao), PARQUE VERDE (Parque Verde, Jardim Cidade Verde e Terra Nova 2), TROPICAL (Cristal e Recanto Tropical), SANTA CRUZ (Santa Cruz, Jardim Santo Onofre, Jardim Fag e Paulo Godoy), CANADA.
	<strong>REGIAO CENTRAL</strong>: CENTRO, COUNTRY, MARIA LUIZA, NEVA (Vila Tolentino).
	<strong>DISTRITOS</strong>: Sede Alvorada, Juvin�polis, Rio do Salto, Sao Joao do Oeste, Espigao Azul e Sao Salvador.
	
A margem de erro � de at� 3% para mais ou para menos.

Observa�ao: Todos os direitos estao reservados a VOXDATA pesquisa e assessoria, estando expressamente proibida a divulga�ao em m�dias sociais escrita, falada ou televisionada dos n�meros constantes nesta amostragem, sem a autoriza�ao do contratante.
");
?>
</div>


<form id="formulario" action="" method="post">
	
<?
	$pesquisa = 60;
	
	$sql = "select * from pergunta where id_pesquisa = $pesquisa and filtro = 1";
	$res = mysql_query($sql);
	while($row = mysql_fetch_assoc($res)){
?>
		<strong><?=$row['pergunta']?></strong><br />

<?
		$id_pergunta = $row['id_pergunta'];
		$sql2 = "select * from resposta where id_pergunta = $id_pergunta";
		
		$res2 = mysql_query($sql2);
		while($row2 = mysql_fetch_assoc($res2)){
			$id_resposta = $row2['id_resposta'];
?>
		<span style="display:inline-block; border:solid silver 1px; font-size:13px; padding:10px 20px; width:180px; margin-top:3px; text-transform:uppercase"><input id="check_<?=$id_resposta?>" name="id_resposta[]" value="<?=$row['radio'] ? "radio_" : ""?><?=$id_resposta?>" type="checkbox" /><label for="check_<?=$id_resposta?>"><?=$row2['resposta']?></label></span>
<?			
		}
?>
<br /><br />
<?		
	}

	
	
	for($i=0; $i<count($arr); $i++){
		$tabela = $arr[$i]['tabela'];
?>		
		<strong><?=$arr[$i]['titulo']?></strong><br />
<?		
		$sql = "select * from $tabela";
		
		$res = mysql_query($sql);
		while($row = mysql_fetch_assoc($res)){
			$id_resposta = $row["id_$tabela"];
?>
		<span style="display:inline-block; border:solid silver 1px; font-size:13px; padding:10px 20px; width:180px; margin-top:3px; text-transform:uppercase"><input id="<?=$tabela."_".$id_resposta?>" name="<?=$tabela?>[]" value="<?=$id_resposta?>" type="checkbox" /><label for="<?=$tabela."_".$id_resposta?>"><?=$row["$tabela"]?></label></span>
<?			
		}
?>
<br /><br />
<?	
	}
?>



	<input style="padding:10px 40px;" type="submit" value="Exibir" />
</form>

<?
	$pesquisa = 60;
?>
<hr />

<?
	$sql = "select * from pergunta where id_pesquisa = $pesquisa  and filtro = 1";
	$res = mysql_query($sql);
	while($row = mysql_fetch_assoc($res)){
		$id_pergunta = $row['id_pergunta'];
		
		$sqlView = "CREATE or REPLACE VIEW radio AS
				select a.id_entrevista, a.id_pesquisa, a.id_atendente, a.id_pergunta, a.id_resposta from entrevista_resultado a inner join entrevista using (id_entrevista)
				where a.id_pesquisa = $pesquisa and a.id_pergunta = $id_pergunta $radio
				group by a.id_entrevista, a.id_pesquisa, a.id_atendente, a.id_pergunta order by a.id_entrevista, a.id_pesquisa, a.id_atendente, a.id_pergunta";
		mysql_query($sqlView);		
?>
<fieldset style="background-color:#F5F5F5">
<h2><?=$row['pergunta']?></h2>
<table border="0" width="100%" cellspacing="0" cellpadding="8">
	<tr>
		<td  align="left" bgcolor="#CCCCCC"><strong>Geral</strong></td>
		<td width="130" align="center" bgcolor="#CCCCCC">%</td>
		<!--<td width="130" align="center" bgcolor="#CCCCCC">Respostas</td>-->
	</tr>
<?
	$sql2 = "select * from resposta where id_pergunta = $id_pergunta $radio";
	$res2 = mysql_query($sql2);
	$total_pesquisas = 0;
	while($row2 = mysql_fetch_assoc($res2)){
		$totalResposta = mysql_num_rows($res2);
		$id_resposta = $row2['id_resposta'];
		if($filtros || $perfil){
			$sql3 = "select * from entrevista_resultado
			left join entrevista using (id_entrevista)
			where 1=1
			$where
			$comp
			and id_entrevista in (select id_entrevista from radio where id_resposta = $id_resposta)  group by id_entrevista";
		} else {
			$sql3 = "select * from radio where id_resposta = $id_resposta";
		}

		$res3 = mysql_query($sql3);
		$total_pesquisas += mysql_num_rows($res3);
	}
	
	//echo $total_pesquisas;
	
	$sql2 = "select * from resposta where id_pergunta = $id_pergunta $radio";
	$res2 = mysql_query($sql2);
	$porcentagemTotal = 0;
	$votosTotal = 0;
	$i=1;
	$legenda = "";
	$porcenagemGrafico = "";
	while($row2 = mysql_fetch_assoc($res2)){
		$totalResposta = mysql_num_rows($res2);
		$id_resposta = $row2['id_resposta'];
		if($filtros || $perfil){
			$sql3 = "select * from entrevista_resultado
			left join entrevista using (id_entrevista)
			where 1=1
			$where
			$comp
			and id_entrevista in (select id_entrevista from radio where id_resposta = $id_resposta)  group by id_entrevista";
		} else {
			$sql3 = "select * from radio where id_resposta = $id_resposta";
		}

		$res3 = mysql_query($sql3);
		$total = mysql_num_rows($res3);
		
		$porcentagem = round($total / $total_pesquisas * 100,1);
		
		if($totalResposta == $i){
			$porcentagem = round(100 - $porcentagemTotal,1);
			$total = $total_pesquisas - $votosTotal;
		}
		$i++;
		
		$votosTotal += $total;
		$porcentagemTotal += $porcentagem;
		
		$porcenagemGrafico .= ", ".$porcentagem;
		
		$legenda .= ", '".$row2['resposta']."'";
?>    
		<tr>
			<td align="" bgcolor="#E1E1E1"><?=$row2['resposta']?></td>
			<td align="center" bgcolor="#FFFFFF" align=""><?= $porcentagem."%"?></td>
			<!--<td bgcolor="#FFFFFF" align="center"><?= $total?></td>-->
		</tr>
<?
	}
	
?>        
	<tr bgcolor="#6C6C6C" style="color:#fff">
		<td align="left" >Total</td>
		<td  align="center"><?= $porcentagemTotal."%"?></td>
		<!--<td  align="center"><?= $votosTotal?></td>-->
	</tr>
    
        
	</tr>    

</table>



</fieldset>


<?
		break;
	}
?>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawVisualization);
  
  function drawVisualization() {
	  var wrapper = new google.visualization.ChartWrapper({
		chartType: 'ColumnChart',
		dataTable: [['' <?=$legenda?>],
					['' <?=$porcenagemGrafico?>]],
		options: {'title': '<?=$row['pergunta']?>'},
		containerId: 'visualization'
	  });
	  wrapper.draw();
	}
</script>
<div id="visualization" style="width: 600px; height: 300px;"></div>

<input style="padding:10px 40px;" onclick="window.print()" type="submit" value="Imprimir" />
</body>
</html>
