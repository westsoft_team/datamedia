<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-2" />
<title>Voxdata - Relatório</title>
<style type="text/css">
.r { text-align: right; }

table tr td { border:solid 1px #333; }

table{
	border-collapse:collapse;
}

.grafico{

}

*{
	font-family:Arial, Helvetica, sans-serif;
}
</style>
</head>
<?
	include_once 'conecta.php';
	
	include_once 'phplot-5.8.0/phplot.php';
	
	
?>


<body>
<form action="" method="post">
	<select style="padding:10px" name="pesquisa">
<?
	$sql = "select * from pesquisa";
	$res = mysql_query($sql);
	while($row = mysql_fetch_assoc($res)){
		$id_pesquisa = $row['id_pesquisa'];
		$pesquisa = $row['descricao'];
		$chk = $_POST['pesquisa'] == $id_pesquisa ? " selected " : "";
		echo "<option $chk value='$id_pesquisa'>$pesquisa</option>";

	}
?>		
	</select>
	<input type="submit" value="Exibir" />
</form>

<?
	$pesquisa = $_POST['pesquisa'];
	
	$sql = "select * from pesquisa where id_pesquisa = $pesquisa";
	$res = mysql_query($sql);
	$row = mysql_fetch_assoc($res);
	$titulo_pesquisa = $row['descricao'];
	
	$sql = "select *
			from entrevista
			inner join entrevista_resultado using (id_entrevista)
			where entrevista.id_pesquisa = $pesquisa group by entrevista.id_entrevista order by id_pergunta";
	$res = mysql_query($sql);
	$total_pesquisas = mysql_num_rows($res);

?>

<h1><?=$titulo_pesquisa?></h1>
<table width="600" bordercolor="#CCCCCC" border="0" cellspacing="0" cellpadding="8">
	<tr>
		<td width="150" bgcolor="#CCCCCC" class="r">Pesquisas realizadas</td>
		<td colspan="2" bgcolor="#CCCCCC"><strong><?=$total_pesquisas?></strong></td>
	</tr>
</table>
<br clear="all" />
<?
	$sql = "select * from pergunta where id_pesquisa = $pesquisa";
	$res = mysql_query($sql);
	while($row = mysql_fetch_assoc($res)){
		$id_pergunta = $row['id_pergunta'];
?>

<table border="0" width="600" cellspacing="0" cellpadding="8">
	<tr>
		<td colspan="8" align="left" bgcolor="#CCCCCC"><strong><?=$row['pergunta']?></strong></td>
	</tr>
<?
	$sql2 = "select * from resposta where id_pergunta = $id_pergunta";
	$res2 = mysql_query($sql2);
	$porcentagemTotal = 0;
	$votosTotal = 0;
	while($row2 = mysql_fetch_assoc($res2)){
		$id_resposta = $row2['id_resposta'];
		$sql3 = "select *
				from entrevista_resultado
				inner join entrevista using (id_entrevista)
				where entrevista_resultado.id_pesquisa = $pesquisa
				and entrevista_resultado.id_resposta = $id_resposta
				group by entrevista_resultado.id_entrevista, entrevista_resultado.id_pergunta
				order by entrevista_resultado.id_entrevista, entrevista_resultado.id_pergunta";
		
		//$sql3 = "select * from entrevista_resultado where id_pesquisa = $pesquisa and id_resposta = $id_resposta group by id_entrevista order by id_pergunta, id_resposta ";
		$res3 = mysql_query($sql3);
		$row3 = mysql_fetch_assoc($res3);
		$total = $row3['total'];
		$total = mysql_num_rows($res3);
		$votosTotal += $total;
		$porcentagem = number_format($total / $total_pesquisas * 100,2);
		$porcentagemTotal += $porcentagem;
		
		/*$milliseconds =  $row3['id_entrevista'];
		$timestamp = $milliseconds/1000;
		echo date("d/m/Y h:m:i", $timestamp);*/
?>    
		<tr>
			<td align="" bgcolor="#E1E1E1"><?=$row2['resposta']?></td><td align=""><?= $porcentagem."%"?></td><td align=""><?= $total?></td>
		</tr>
<?
	}
?>        
	<tr bgcolor="#6C6C6C" style="color:#fff">
		<td align="" >Total</td>
		<td  align=""><?= $porcentagemTotal."%"?></td>
		<td align=""><?= $votosTotal?></td>
	</tr>
    
        
	</tr>    

</table>
<br clear="all" /><br clear="all" />
<?
	}
	
	function converterData($data){
	if (strstr($data, "/")){
		$A = explode ("/", $data);
		$V_data = $A[2] . "-". $A[1] . "-" . $A[0];
	} else {
		$A = explode ("-", $data);
		$V_data = $A[2] . "/". $A[1] . "/" . $A[0];
	}
	return $V_data;
}
	
?>

</body>
</html>
